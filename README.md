# Shopper's Voice - ALK allergy season

Shopper's voice templates are included, but our portion lives in separate files. To be dropped into the space between    
```
<!--  Start Client Content  -->



<!--  End Client Content  -->
```

We are not using Silverpop to deploy this email, so images are currently using local `src` paths. 

Images are hosted in the asset library on Silverpop for initial QA purposes and used as:
```
<img src="hero.jpg" name="hero.jpg" contentid=72d608ab-15f77f97799-1973771dea71da7e4c551ed9f05528be xt="SPIMAGE" SPNAME="hero.jpg" width="600" alt="" border="0" style="width:100%;height:auto;display:block;"/>
```

Where `contentid` is created by silverpop. 